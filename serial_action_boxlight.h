#pragma once

namespace SerialUSB
{
	namespace Actions // oh boy do i like to see c++17 in arduino
	{
		class BoxLight : public Base
		{
			private:
				int val = 0;

			public:
				BoxLight() : Base("BL"){}

				String handle(String data);
		};
	};
};