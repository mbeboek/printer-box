#include "main_header.h"

Program::Program()
: thermo(Thermistor(A1, 4700)), buttonLight(Button(8)), buttonPrinter(Button(9)), buttonFan(Button(2)), buttonMode(Button(3)),
pinRelay(Pin(PIN_RELAY)), pinFan(Pin(PIN_FAN)), pinLight(Pin(PIN_LIGHT)), pinThermoPower(Pin(PIN_POWER_THERMISTOR))
{
}

void Program::init()
{
	pinRelay.set(false);
	pinFan.set(false);
	pinLight.set(false);
	pinThermoPower.set(true);

	buttonLight.init();
	buttonPrinter.init();
	buttonFan.init();
	buttonMode.init();

	oled.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
	oled.ssd1306_command(SSD1306_SETCONTRAST);
	oled.ssd1306_command(200);
	oled.display(); // show splashscreen

	Serial.begin(9600);

	serialHandler.addAction(new SerialUSB::Actions::BoxLight());
	serialHandler.addAction(new SerialUSB::Actions::BoxTemp());
	serialHandler.addAction(new SerialUSB::Actions::Fan());
	serialHandler.addAction(new SerialUSB::Actions::Printer());
}

void Program::mainLoop()
{
	auto currTime = millis();

	update();

	if((timeDraw + DRAW_TIME) < currTime)
	{
		draw();
		timeDraw = currTime;
	}
}

void Program::update()
{
	buttonLight.update();
	buttonPrinter.update();
	buttonFan.update();
	buttonMode.update();

	if(buttonLight.press())
		pinLight.set(true);

	if(buttonPrinter.press())
		pinRelay.set(true);

	if(buttonFan.press())
		pinFan.set(true);

	if(buttonMode.press())
	{
		pinLight.set(false);
		pinRelay.set(false);
		pinFan.set(false);
	}

	serialHandler.update();
}

#include "icons/fan_01.h"
#include "icons/fan_02.h"
#include "icons/fan_03.h"
#include "icons/3d_printer.h"
#include "icons/icon_x.h"

void Program::draw()
{
	oled.clearDisplay();
	oled.setCursor(0, 0);

	oled.setTextSize(1);
	oled.setTextColor(WHITE);
	oled.setCursor(0, 0);

	oled.drawRect(0, 0, 33, 32, WHITE);
	oled.drawRect(0, 0, 64, 32, WHITE);
	oled.drawRect(64, 0, 64, 32, WHITE);

	oled.setTextSize(1);
	oled.setCursor(34, 1);

	oled.setCursor(70, 6);
	oled.setTextSize(3);

	oled.print((int)thermo.temp());
	oled.print("C");

	oled.setCursor(0, 34);
	oled.setTextSize(1);

	for(int i=0; i<4; ++i)
	{
		oled.println(serialHandler.cmdHistory[i]);
	}

	auto currTime = millis();
	int imgFrame = (currTime / 120) % 3;

	if(pinFan.val())
	{
		if(imgFrame == 0)
			oled.drawBitmap(0, 0, fan_01, 32, 32, WHITE);
		else if(imgFrame == 1)
			oled.drawBitmap(0, 0, fan_02, 32, 32, WHITE);
		else if(imgFrame == 2)
			oled.drawBitmap(0, 0, fan_03, 32, 32, WHITE);
	}else{
		oled.drawBitmap(0, 0, icon_x, 32, 32, WHITE);
	}

	if(pinRelay.val())
	{
		oled.drawBitmap(32, 0, img_3d_printer, 32, 32, WHITE);
	}else{
		oled.drawBitmap(32, 0, icon_x, 32, 32, WHITE);
	}

	oled.display();
}
