#include <Arduino.h>
#include "button.h"

Button::Button(int pin)
: pin(pin), state(STATE_NEUTRAL)
{
}

void Button::init()
{
	pinMode(pin, INPUT);
}

void Button::update()
{
	if(digitalRead(pin) == HIGH)
	{
		if(state == STATE_NEUTRAL || state == STATE_RELEASE)
		{
			state = STATE_NEWPRESS;
		}else if(state == STATE_NEWPRESS)
		{
			state = STATE_DOWN;
		}
	}else{
		state = (state == STATE_RELEASE) ? STATE_NEUTRAL : STATE_RELEASE;
	}
}

bool Button::down()
{
	return state == STATE_NEWPRESS || state == STATE_DOWN;
}

bool Button::press()
{
	return state == STATE_NEWPRESS;
}
