#pragma once

namespace SerialUSB
{
	namespace Actions // oh boy do i like to see c++17 in arduino
	{
		class Fan : public Base
		{
			private:
				int val = 0;

			public:
				Fan() : Base("FN"){}

				String handle(String data);
		};
	};
};