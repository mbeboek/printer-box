/**
 * @author Max Beb�k
 */

#include "main_header.h"

Program mainProg = Program();

void setup() 
{
	mainProg.init();
}

void loop() 
{
	mainProg.mainLoop();
}
