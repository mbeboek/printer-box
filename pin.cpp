#include "main_header.h"

Pin::Pin(int pin, bool output)
: pin(pin), isOutput(output)
{
	pinMode(pin, isOutput ? OUTPUT : INPUT);
}

void Pin::set(int val)
{
	analogWrite(pin, value = val);
}

void Pin::set(bool val)
{
	digitalWrite(pin, (value = val) ? HIGH : LOW);
}

int Pin::read()
{
	return digitalRead(pin);
}

int Pin::val()
{
	return value;
}
