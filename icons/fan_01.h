//------------------------------------------------------------------------------
// File generated by LCD Assistant
// http://en.radzio.dxp.pl/bitmap_converter/
//------------------------------------------------------------------------------

const unsigned char PROGMEM fan_01 [] = {
0x80, 0x00, 0x00, 0x01, 0x20, 0x00, 0x00, 0x04, 0x60, 0x0F, 0xF0, 0x06, 0x00, 0x38, 0x1C, 0x00,
0x00, 0xF8, 0x1F, 0x00, 0x01, 0xF8, 0x3F, 0x80, 0x03, 0xF8, 0x3F, 0xC0, 0x07, 0xF8, 0x7E, 0x60,
0x0D, 0xF8, 0x7C, 0x30, 0x0C, 0xF8, 0xF0, 0x30, 0x18, 0x78, 0xE0, 0x18, 0x10, 0x38, 0x00, 0x08,
0x20, 0x10, 0x00, 0x3C, 0x38, 0x00, 0x07, 0xFC, 0x3E, 0x00, 0x07, 0xFC, 0x3F, 0x81, 0xC7, 0xFC,
0x3F, 0xE1, 0xC1, 0xFC, 0x3F, 0xE1, 0xC0, 0x7C, 0x3F, 0xE0, 0x00, 0x1C, 0x3E, 0x00, 0x08, 0x04,
0x10, 0x00, 0x1C, 0x08, 0x18, 0x07, 0x1E, 0x18, 0x0C, 0x0F, 0x1F, 0x38, 0x0C, 0x3E, 0x1F, 0xB0,
0x06, 0x7E, 0x1F, 0xE0, 0x03, 0xFE, 0x1F, 0xC0, 0x01, 0xFC, 0x1F, 0x80, 0x00, 0xFC, 0x1F, 0x00,
0x00, 0x38, 0x1E, 0x00, 0x60, 0x0F, 0xF0, 0x06, 0x20, 0x00, 0x00, 0x04, 0x80, 0x00, 0x00, 0x01
};
