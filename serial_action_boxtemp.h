#pragma once

namespace SerialUSB
{
	namespace Actions
	{
		class BoxTemp : public Base
		{
		private:

		public:
			BoxTemp() : Base("BT"){}

			String handle(String data);
		};
	};
};
