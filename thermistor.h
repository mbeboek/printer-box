#pragma once

class Thermistor
{
	private:
		int pin;
		int resistorVal;

		float sensorNominal;
		float tempNominal;
		float coefficient;

	public:
		Thermistor(int pin, int resistorVal, float sensorNominal = 100000.0f, float tempNominal = 25.0f, float coefficient = 3950.0f);

		float temp();

};