#pragma once

namespace SerialUSB
{
	struct Command
	{
		char type[2];
		String data;

		void clear()
		{
			type[0] = 0;
			type[1] = 0;
			data    = "";
		}
	};
};