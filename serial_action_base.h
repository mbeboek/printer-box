#pragma once

namespace SerialUSB
{
	namespace Actions
	{
		class Base
		{
			private:

			protected:
				char type[2];

			public:
				Base(char* t);
				bool checkType(char _type[2]);

				virtual String handle(String data);
		};
	}
};