#pragma once

class Button
{
	private:
		const int pin;
		int state = 0;

		const static int STATE_NEUTRAL  = 0;
		const static int STATE_NEWPRESS = 1;
		const static int STATE_DOWN     = 2;
		const static int STATE_RELEASE  = 3;

	public:
		Button(int pin);

		void init();
		void update();

		bool down();
		bool press();
};