#include "main_header.h"

String SerialUSB::Actions::BoxLight::handle(String data)
{
	if(data[0] != '%')
	{
		val = data.toInt();
		if(val > 255)val = 255;
		if(val < 0)val = 0;

		mainProg.pinLight.set(val);
	}

	return String(val);
}
