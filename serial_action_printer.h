#pragma once

namespace SerialUSB
{
	namespace Actions // oh boy do i like to see c++17 in arduino
	{
		class Printer : public Base
		{
		private:
			bool isOn = false;

		public:
			Printer() : Base("PR"){}

			String handle(String data);
		};
	};
};