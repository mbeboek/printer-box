#pragma once
// base
#include "serial_action_base.h"
// types
#include "serial_action_boxlight.h"
#include "serial_action_boxtemp.h"
#include "serial_action_fan.h"
#include "serial_action_printer.h"

#define MAX_ACTION_SLOTS 5
#define NUM_CMD_HISTORY 6

namespace SerialUSB
{
	/**
	 * Handles serial input and calls the action classes
	 */
	class Handler
	{
		private:
			int numActions = 0;
			Actions::Base* actions[MAX_ACTION_SLOTS];

			Command cmd;

			void processCommand();

		public:
			String cmdHistory[NUM_CMD_HISTORY];

			Handler();
			~Handler();

			void addToHistory(String txt);
			void addAction(Actions::Base* action);
			void update();

			void clear();
	};
};