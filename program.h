#pragma once

class Program
{
	private:
		const static int DRAW_TIME = 1000 / 60;
		unsigned long timeDraw;

		void update();
		void draw();

	public:
		Adafruit_SSD1306 oled;
		SerialUSB::Handler serialHandler;
		Thermistor thermo;

		Button buttonLight;
		Button buttonPrinter;
		Button buttonFan;
		Button buttonMode;

		Pin pinRelay;
		Pin pinFan;
		Pin pinLight;
		Pin pinThermoPower;

		Program();
		void init();
		void mainLoop();
};