#pragma once
/*************************
 * 3D Printer Controller *
 * @author Max Beb�k     *
 *************************/

#define PIN_POWER_THERMISTOR 4
#define PIN_RELAY 5
#define PIN_LIGHT 6
#define PIN_FAN   7

//########// external libs //########//
#include <Arduino.h>
#include "vector\vector.h"
#include "Adafruit_SSD1306/Adafruit_SSD1306.h"

//########// own header //########//
#include "pin.h"
#include "thermistor.h"
#include "button.h"

#include "command.h"
#include "serial_handler.h"

#include "program.h"

//########// globals //########//
extern Program mainProg;