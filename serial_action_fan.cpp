#include "main_header.h"

String SerialUSB::Actions::Fan::handle(String data)
{
	if(data[0] != '%')
	{
		val = data.toInt() > 0 ? 1 : 0;
		mainProg.pinFan.set((bool)val);
	}

	return String(val);
}
