#include "main_header.h"

#define NUMSAMPLES 50

Thermistor::Thermistor(int pin, int resistorVal, float sensorNominal, float tempNominal, float coefficient)
	: pin(pin), resistorVal(resistorVal), sensorNominal(sensorNominal), tempNominal(tempNominal), coefficient(coefficient)
{
}

float Thermistor::temp()
{
	float avg = 0.0;

	for(int i=0; i< NUMSAMPLES; i++) {
		avg += analogRead(pin);
		delayMicroseconds(50);
	}
	avg /= NUMSAMPLES;
	
	// convert the value to resistance
	avg = 1023 / avg - 1;
	avg = resistorVal / avg;

	float steinhart;
	steinhart = avg / sensorNominal;     // (R/Ro)
	steinhart = log(steinhart);                  // ln(R/Ro)
	steinhart /= coefficient;                   // 1/B * ln(R/Ro)
	steinhart += 1.0 / ((float)tempNominal + 273.15); // + (1/To)
	steinhart = 1.0 / steinhart;                 // Invert
	steinhart -= 273.15;                         // convert to C
	
	return steinhart;
}
