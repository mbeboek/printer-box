#pragma once

class Pin
{
	private:
		int pin;
		bool isOutput;
		int value = 0;

	public:
		Pin(int pin, bool output = true);

		void set(int val);
		void set(bool val);
		int read();
		int val();
};