#include "main_header.h"

SerialUSB::Actions::Base::Base(char *t)
{
	type[0] = t[0];
	type[1] = t[1];
}

bool SerialUSB::Actions::Base::checkType(char _type[2])
{
	return type[0] == _type[0] && type[1] == _type[1];
}
