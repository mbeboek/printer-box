#include "main_header.h"

String SerialUSB::Actions::Printer::handle(String data)
{
	if(data[0] == '1')
		isOn = true;
	else if(data[0] == '0')
		isOn = false;

	mainProg.pinRelay.set(isOn);

	return isOn ? "1" : "0";
}
