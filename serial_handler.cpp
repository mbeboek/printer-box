#include "main_header.h"

void SerialUSB::Handler::processCommand()
{
	if(cmd.type[0] != 0 || cmd.type[1] != 0)
	{
		for(int i=0; i<numActions; ++i)
		{
			if(actions[i]->checkType(cmd.type))
			{
				addToHistory(String("USB: ") + cmd.type[0] + cmd.type[1] + String("->'") + cmd.data + String("'"));

				String result = actions[i]->handle(cmd.data);
				result += '#';
				Serial.write(result.c_str());

				break;
			}
			if(i == numActions-1)
			{
				addToHistory(String("USB: unknown type: ") + cmd.type[0] + cmd.type[1]);
			}
		}
	}

	cmd.clear();
}

SerialUSB::Handler::Handler()
{
	cmd.clear();
}

SerialUSB::Handler::~Handler()
{
	clear();
}

void SerialUSB::Handler::addToHistory(String txt)
{
	for(int i=NUM_CMD_HISTORY-1; i>0; --i)
	{
		cmdHistory[i] = cmdHistory[i-1];
	}
	cmdHistory[0] = txt;
}

void SerialUSB::Handler::addAction(SerialUSB::Actions::Base * action)
{
	if(numActions != MAX_ACTION_SLOTS)
	{
		actions[numActions++] = action;
	}
}

void SerialUSB::Handler::update()
{
	while(Serial.available() > 0)
	{
		char c = Serial.read();
		//Serial.println(String((int)c) + String(" - ") + c);
		if(c == '#' || c == 0)
		{
			processCommand();
		} 
		else if(cmd.type[0] == 0)cmd.type[0] = c;
		else if(cmd.type[1] == 0)cmd.type[1] = c;
		else cmd.data += c;
	}
}

void SerialUSB::Handler::clear()
{
	cmd.clear();

	for(int i=0; i<NUM_CMD_HISTORY; ++i)
	{
		cmdHistory[i] = "";
	}

	for(int i=0; i<numActions; ++i)
		delete actions[i];

	numActions = 0;
}
